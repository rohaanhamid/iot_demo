var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
var errorhandler = require('errorhandler');
var path = require('path');
var mongoose = require('mongoose');

var app = express();
var http = require('http').Server(app);
var io = require('./routes/socket/iomain').listen(http);

var routes_index = require('./routes/index');
var routes_devices = require('./routes/api/devices');
var routes_clients = require('./routes/api/clients');

// Setup mongoose
mongoose.connect('mongodb://localhost/db_qnx_assignment', function(err) {
    if(err) {
        console.log('MongoDB Connection Error', err);
    } else {
        console.log('MongoDB Connection Successful');
        mongoose.connection.db.dropCollection('clients', function(err, result) {
            console.log(result);
        if(!err){
            console.log('Clients collection dropped');
        }
     });
    }
});

// view engine setup *REMOVE IF ANGULAR IS USED*
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, '/public'))); //Serve Static Assets from the public folder.

// setup all the routes
app.use('/api', routes_devices);
app.use('/api', routes_clients);
app.use('/', routes_index); //Contains catch all route to redirect unknown requests to index.html

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

if (process.env.NODE_ENV === 'development') {
  // only use in development
  app.use(errorhandler())
}

http.listen(3000, function(){
  console.log('Server listening on *:3000');
});

module.exports = app;