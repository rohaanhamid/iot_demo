var mongoose = require('mongoose');

// This table holds information on the existing clients and the device they subscribe to.
// The client type is required to identify the system overview page.

var ClientSchema = new mongoose.Schema({
      name: {type: String, required: true, unique: true},
      type: {type: String, required: true},
      socket_id: {type: String, required: true, unique: true},
      subscribed_device: {type: String} //Cross-references Device ids. One device only for simplicity
});

module.exports = mongoose.model('Client', ClientSchema);