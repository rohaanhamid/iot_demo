var mongoose = require('mongoose');

/*Possible States are numbers from 0 to 5*/

var DeviceSchema = new mongoose.Schema({
  name: {type: String, unique: true, required: true},
  state: {type: Number, default: 0},
  subscribing_client: {type: String} // Store SocketIO ids for client subscribing to this device
});

module.exports = mongoose.model('Device', DeviceSchema); 