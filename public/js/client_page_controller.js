var client_socket = io('http://localhost:3000/clients');
var device_socket = io('http://localhost:3000/devices');

device_socket.on('device_added', function(data){
    appendUpdatesList("System Info: New device added to the system with name: " + data.name);
    appendDeviceSelector(data)
});

device_socket.on('device_removed', function(data){
    appendUpdatesList("System Info: Device " + data.name + ' was removed from the system');
    removeFromSelector(data);
});

client_socket.on('subscribed_device_state_changed', function(data){
    appendUpdatesList("State of device: " + data.name + " changed to " + data.state);
});

function populateDeviceSelector(devices) {
    $.each(devices, function(i, device) { 
        appendDeviceSelector(device)                          
    });
}

function appendDeviceSelector(device) {
    $('#deviceSelector').append('<option value="' + device._id + '">'+device.name+'</option>');
}

function removeFromSelector(device) {
    $('#deviceSelector option[value=' + device._id + ']').remove();
}

function appendUpdatesList(string){
    $('#updatesList').append('<li class="list-group-item">' + string + '</li>');
}

$(document).ready(function() {
    httpGetAllDevices(populateDeviceSelector);
    $("#deviceWatchContainer").hide();
    $( "#submitClientName" ).click(function() {
        var clientName = $('#textClientName').val();
        if(clientName == '') {
            alert("Name can't be empty");     
        }else{
            var client = {
                name : clientName, 
                type: 'client_app',
                socket_id: client_socket.id
            }
            httpPostClient(client, function(res){
                if(res.message == "failure"){
                    alert("Failure" + res.data);
                }
                else{
                 alert("Success");
                    $("#deviceWatchContainer").show();
                }
            });
        }
    });
    
    $( "#deviceSelector" ).change(function() {
        var currentDevice  = $("#deviceSelector option:selected").text();
        var currentDeviceId  = $("#deviceSelector option:selected").val();

        var data = {
            socket_id : client_socket.id,
            device_id: currentDeviceId
        };
        httpPutClientSubscribedDevice(data, function(res_client){
                //appendUpdatesList('Listening to device: ' + currentDevice);
            httpGetDevicesById(currentDeviceId, function(res_device){
                appendUpdatesList('Listening to device: ' + res_device.data.name + ' current state: ' + res_device.data.state);
            });
                
        });
    });

});

window.onbeforeunload = function() { 
   httpDeleteClient({socket_id : client_socket.id});
}