var device_socket = io('http://localhost:3000/devices');
var client_socket = io('http://localhost:3000/clients');

client_socket.on("client_added", appendClientsTable);
client_socket.on("client_removed", removeClientFromTable);
client_socket.on("client_subscribed_to_device", function(res){
    console.log(res);
    $('#' + res.subscribing_client + ' #subscribedDevice').text(res.name);    
});


function appendClientsTable(client){
    if(client.type != 'system_overview'){
        $('#clientsTable').append('<tr id="' + client.socket_id +'"><td>' + client.name+'</td><td id="subscribedDevice">None</td></tr>');        
    }
}

function appendDevicesTable(device){
    $('#devicesTable').append('<tr id="' + device._id +'"><td>'+device.name+'</td><td id="state">'+ device.state +'</td></tr>');                
}

function populateClientsTable(clients) {
    $.each(clients, function(i, client) { 
        appendClientsTable(client)                      
    });
}

function removeClientFromTable(client) {
    $('#' + client.socket_id).remove();           
}

function populateDevicesTable(devices) {
    $.each(devices, function(i, device) { 
        appendDevicesTable(device);
    });    
}

function populateDeviceSelector(devices) {
    console.log(devices);
    $('#deviceSelector').empty();
    $.each(devices, function(i, device) { 
        $('#deviceSelector').append('<option value="' + device._id + '">'+device.name+'</option>');                           });
}

$('#addDeviceButton').click(function(){
    var name = $("#nameField").val();
    var state = $("#addDeviceModal #stateSelector").val();

    if(name == ""){
        alert("Name can't be empty");
    }

    var data = {
        name: name,
        state: state
    };


    httpPostDevice(data, function(res){
        if(res.message == "success"){
            alert("Success");
            appendDevicesTable(res.data);
        }else{
            alert("Failure: " + res.data);
        }
    });

});

$(document).ready(function() {

    httpGetAllDevices(populateDevicesTable);
    httpGetAllClients(populateClientsTable);
    
    $('#addDeviceButton').click(function(){
    var name = $("#nameField").val();
    var state = $("#addDeviceModal #stateSelector").val();

    if(name == ""){
        alert("Name can't be empty");
    }

    var data = {
        name: name,
        state: state
    };


    httpPostDevice(data, function(res){
        if(res.message == "success"){
            alert("Success");
            appendDevicesTable(res.data);
        }else{
            alert("Failure: " + res.data);
        }
    });

    });
    
    $('#manageDeviceModal').on('shown.bs.modal', function() {
        httpGetAllDevices(populateDeviceSelector);
    });
    
    $('#setStateButton').click(function() {
        var data = {
            state : $('#deviceStateSelector').val(),
            _id : $('#deviceSelector').val()
        }
        httpPutDeviceState(data, function(res){
            $('#devicesTable #'+ res._id + ' #state').text(res.state);
        });
    });
    
    $('#removeDeviceButton').click(function() {
        var id = $('#deviceSelector').val()
        httpDeleteDevice(id, function(res){
            $('#devicesTable #' + res._id).remove();      
        });
        $('#deviceSelector option[value=' + id + ']').remove();
    });
    
});