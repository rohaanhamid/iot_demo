/* Endpoint /api/devices*/
function httpGetAllDevices(callback){
    $.ajax({
        url: "/api/devices"
    }).then(callback);
}

/* Endpoint /api/devices*/
function httpGetDevicesById(id, callback){
    $.ajax({
        url: "/api/devices/" + id
    }).then(callback);
}

function httpPostDevice(data, callback){
    $.ajax({
        url        : '/api/devices',
        dataType   : 'json',
        contentType: 'application/json; charset=UTF-8', 
        data       : JSON.stringify(data),
        type       : 'POST',
        success    : callback
    });
}

function httpPutDeviceState(data, callback){
    $.ajax({
        url        : '/api/devices/' + data._id + '/state',
        dataType   : 'json',
        contentType: 'application/json; charset=UTF-8', 
        data       : JSON.stringify(data),
        type       : 'PUT',
        success    : callback
    });
}

function httpDeleteDevice(id, callback){
    $.ajax({
        url        : '/api/devices/' + id,
        type       : 'DELETE',
        success    : callback
    });
}

function httpPostDevice(data, callback){
    $.ajax({
        url        : '/api/devices',
        dataType   : 'json',
        contentType: 'application/json; charset=UTF-8', 
        data       : JSON.stringify(data),
        type       : 'POST',
        success    : callback
    });
}

/* Endpoint /api/clients*/

function httpGetAllClients(callback){
    $.ajax({
        url: "/api/clients"
    }).then(callback);
}

function httpPostClient(data, callback){
    $.ajax({
        url        : '/api/clients',
        dataType   : 'json',
        contentType: 'application/json; charset=UTF-8', 
        data       : JSON.stringify(data),
        type       : 'POST',
        success    : callback
    });
}

function httpDeleteClient(data, callback){
    $.ajax({
        url        : '/api/clients/' + data.socket_id,
        type       : 'DELETE',
        success    : callback
    });
}

function httpPutClientSubscribedDevice(data, callback){
    $.ajax({
        url        : '/api/clients/' + data.socket_id + '/subscribed_device',
        dataType   : 'json',
        contentType: 'application/json; charset=UTF-8', 
        data       : JSON.stringify(data),
        type       : 'PUT',
        success    : callback
    });
}
