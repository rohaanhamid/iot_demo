var express = require('express');
var router = express.Router();
var app = require('../../app'); //Contains the io variable

var client_model = require("../../models/client"); 
var device_model = require("../../models/device"); 

/* GET all clients. */
router.get('/clients', function (req, res) {  
    client_model.find({}, function(err, docs) {
            if (!err){ 
               res.json(docs);
            }
            else { throw err;}
        }
    );
});

/* POST a new client and return its model. */
router.post('/clients', function (req, res) {    
    var client = new client_model();
    
    console.log(req.body);
    
    client.name = req.body.name;
    client.socket_id = req.body.socket_id;
    client.type = req.body.type;

    client.save(function(err) {
        if (err){
            res.json({message: 'failure', data: err.err});
            console.log(err);
        }else{
            console.log('Success');
            io.of('/clients').emit('client_added', client); //Broadcast event to all clients
            res.json({ message: 'success', data: client});
        }
    });
});

/* GET a client by ID. */
router.get('/clients/:id', function (req, res) {    
    
});

/* UPDATE a client's subscribed devices. */
router.put('/clients/:socket_id/subscribed_device', function (req, res) {
    var socket_id = req.params.socket_id;
    var device_id = req.body.device_id;
    
    console.log(device_id);
    
    client_model.findOne({ socket_id : socket_id}, function (err, doc_client){
      doc_client.subscribed_device = device_id;
      doc_client.save();
      device_model.findOne({ _id : device_id }, function(err, doc_device){
          console.log(doc_device);
            doc_device.subscribing_client = doc_client.socket_id;
            io.of('/clients').emit('client_subscribed_to_device', doc_device); //Intentionally a broadcast event
      });
      res.json(doc_client);
    });
});

/* DELETE device by Socket ID. */
router.delete('/clients/:socket_id', function (req, res) {    
    var socket_id =  req.params.socket_id;
    console.log('Remove: ' + socket_id);
    client_model.findOne({ socket_id : socket_id }, function(err, doc) {
        if(!err && doc!=null){
            io.of('/clients').emit('client_removed', doc);
            res.json(doc);
            doc.remove();
        }
        
    });
    
});

module.exports = router;