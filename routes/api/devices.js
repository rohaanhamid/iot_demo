var express = require('express');
var router = express.Router();
var device_model = require("../../models/device"); 
var client_model = require("../../models/client"); 
var app = require('../../app'); //Contains the io variable

/* GET all devices. */
router.get('/devices', function (req, res) {    
    device_model.find({}, function(err, docs) {
            if (!err){ 
               res.json(docs);
                      }
            else { throw err;}
            }
    );
});

/* POST a new device and return its model. */
router.post('/devices', function (req, res) {    
    var device = new device_model();
    
    console.log(req.body);
    
    device.name = req.body.name;
    device.state = parseInt(req.body.state);

    device.save(function(err) {
        console.log(err);
        if (err){
            res.json({message: 'failure', data: err.err});
        }else{
            io.of('/devices').emit('device_added', device); //Broadcast event to all clients
            res.json({ message: 'success', data: device });
        }
    });
});

/* GET a device by ID. */
router.get('/devices/:id', function (req, res) { 
     var id =  req.params.id;
     device_model.findOne({ _id : id }, function (err, doc){
        if(err){
            res.json({message: 'failure', data: err.err});
        }else{
            res.json({ message: 'success', data: doc });
        }
    });
});

/* UPDATE State of a device by ID. */
router.put('/devices/:id/state', function (req, res) {    
    var id =  req.params.id;
    var state = parseInt(req.body.state);
    
    console.log(req.body);
    
    device_model.findOne({ _id : id }, function (err, doc){
        if(!err){
            doc.state = state;
            doc.save();
            
            client_model.find({'subscribed_device' : id}, function(err, docs){
                console.log('error' +  err);
                console.log('docs' +  docs);
                for(var i=0; i < docs.length; i++){
                    //notify state to only the clients that subscribe to the device
                    io.of('/clients').connected[docs[i].socket_id].emit('subscribed_device_state_changed', doc);
                }
            });
            console.log(doc);
            res.json(doc);
        }else{
            res.json(err);
        }
    });
    
});

/* DELETE device by ID. */
router.delete('/devices/:id', function (req, res) {    
    var id =  req.params.id;
    
    device_model.findOne({ _id : id }, function(err, doc) {
        if(!err){
            io.of('/devices').emit('device_removed', doc);
            res.json(doc);
            doc.remove();
        }
        
    });
    
});

module.exports = router;