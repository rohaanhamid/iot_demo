var express = require('express');
var router = express.Router();

/* GET System Overview Page GET and redirect to statically served html */
router.get('/', function (req, res) {    
    console.log("hit");
    //res.send('public', indeddd.html);
    res.redirect('index.html');
    
});

/* GET Client App Page and redirect to statically served html */
router.get('/client', function (req, res) { 
    res.redirect('client.html');
});

/* GET Client App Page and redirect to statically served html */
router.get('*', function (req, res) { 
    res.redirect('index.html');
});



module.exports = router;