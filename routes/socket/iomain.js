var socketio = require('socket.io');
var client_model = require("../../models/client"); 

module.exports.listen = function(app){
    
    io = socketio.listen(app)

    io.set('origins', '*:*');

    io.on('connection', function(socket){
        //console.log('a client connected');
        //console.log(socket.id);
    });
    
    io.of('/clients')
        .on('connection', function(socket){
            console.log('A socket was connected to "/clients" with id: ' + socket.id);
    });
    
    io.of('/devices')
        .on('connection', function(socket){
            console.log('A socket was connected to "/devices" with id: ' + socket.id);
    });
    
    return io;
}